package com.project.AdmoDb.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<LikedMovie, Integer> {


    List<LikedMovie> findByUserId(int userId);

    LikedMovie findByMovieIdAndUserId(String movieId, int userId);

}