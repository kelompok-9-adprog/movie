package com.project.AdmoDb.entity;

import java.util.List;

public class LikedMovieResponse {
    private List<LikedMovie> likedMovies;

    public LikedMovieResponse() {}

    public LikedMovieResponse(List<LikedMovie> movies) {
        this.likedMovies = movies;
    }

    public List<LikedMovie> getLikedMovies() {
        return this.likedMovies;
    }
}
