package com.project.AdmoDb.entity;

import javax.persistence.*;

@Entity
@Table (name = "liked")
public class LikedMovie {

    @Id
    @Column(name = "movieid")
    private String movieId;

    @Column(name = "userid")
    private int userId;

    public int getUserId() {
        return userId;
    }

    public String getMovieId() {
        return movieId;
    }

    public LikedMovie() {
    }

    public LikedMovie(String movieId, int userId){

        this.movieId = movieId;
        this.userId = userId;
    }

}
