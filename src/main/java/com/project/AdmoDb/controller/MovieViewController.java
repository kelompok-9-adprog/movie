package com.project.AdmoDb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MovieViewController {

    @GetMapping("/genrelist")
    public String getAllGenreNames(){
        return "genre";
    }

    @GetMapping("/movielist")
    public String getMoviesByGenre(@RequestParam(name = "genre") String genre, @RequestParam(name = "page") String page){
        return "movieResult";
    }

    @GetMapping("/movie")
    public String getMovieById(@RequestParam(name = "id") String id) {
        return "movie";
    }

    @GetMapping("/likedmovie")
    public String getLikedMoviesByUserId(@RequestParam(name = "userid") String id) {
        return "likedmovie";
    }
}
