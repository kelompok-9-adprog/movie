var genreIds = new Array();
var genreNames = new Array();
var genreArray = new Array();
var movieGenreIds = new Array();

$(document).ready(function(){

    fillGenreArray();
    getLikedMovieResult();

    function getURLParameter(param){
        var pageURL = window.location.search.substring(1);
        var URLVariables = pageURL.split('&');
        for (var i = 0; i < URLVariables.length; i++)
        {
            var parameterName = URLVariables[i].split('=');
            if (parameterName[0] == param)
            {
                return parameterName[1];
            }
        }
    }

    function getLikedMovieResult() {
        $(function() {
            var userId = getURLParameter('userid');
            $.ajax({
                type: 'GET',
                url: 'getlikedmovie?userid=' + userId,
                dataType: 'json',
                success: function(result){
                    if (result.likedMovies.length != 0) {
                        $('#liked').hide();
                        $('#likedmovietable').show();
                        $.each(result.likedMovies, function(i, likedMovie) {
                            getIndividualLikedMovie(likedMovie.movieId);
                        });
                    } else {
                        $('#likedmovietable').hide();
                        $('<div>').text("You have not liked any movie").appendTo('#liked');
                        $('#liked').show();
                    }
                }
            });
        });
    }

    function getIndividualLikedMovie(movieId) {
        $(function() {
            $.ajax({
                type: 'GET',
                url: 'getmovie?id=' + movieId,
                dataType: 'json',
                success: function(movie){
                    $.each(movie.genres, function(i, genre) {
                        movieGenreIds.push(genre.id);
                    });
                    var thisGenre = getMovieGenreName(movieGenreIds);
                    $('<tr>').append(
                        $('<td>').append('<img src="http://image.tmdb.org/t/p/w185/' + movie.poster_path + '">'),
                        $('<td>').append('<a href="movie?id=' + movie.id + '">' + movie.title + '</a>'),
                        $('<td>').text(movie.vote_average),
                        $('<td>').text(thisGenre),
                        $('<td>').text(movie.overview),
                    ).appendTo('#likedmovielist');
                }
            });
        });
    }

    function fillGenreArray() {
        $(function() {
            $.ajax({
                type: 'GET',
                url: 'getgenrelist',
                dataType: 'json',
                success: function(result){
                    $.each(result.genres, function(i, genre) {
                        var genrePair = new Array();
                        genreIds[i] = genre.id;
                        genreNames[i] = genre.name;
                        genrePair[0] = genreIds[i];
                        genrePair[1] = genreNames[i];
                        genreArray.push(genrePair);
                    });
                }
            });
        });
    }

    function getMovieGenreName(movieGenreId) {
        var movieGenreStr = "";
        for (i = 0; i < movieGenreId.length; i++) {
            for (j = 0; j < genreArray.length; j++) {
                if (genreArray[j][0] == movieGenreId[i]) {
                    movieGenreStr += genreArray[j][1];
                }
            }
            if (i != movieGenreId.length - 1) {
                movieGenreStr += ", ";
            }
        }
        return movieGenreStr;
    }

});