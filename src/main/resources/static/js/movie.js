var genreIds = new Array();
var genreNames = new Array();
var genreArray = new Array();
var movieGenreIds = new Array();
likedMovieExist = false;

$(document).ready(function(){

    fillGenreArray();
    getSingleMovieResult();

    function getURLParameter(param){
        var pageURL = window.location.search.substring(1);
        var URLVariables = pageURL.split('&');
        for (var i = 0; i < URLVariables.length; i++)
        {
            var parameterName = URLVariables[i].split('=');
            if (parameterName[0] == param)
            {
                return parameterName[1];
            }
        }
    }

    function getSingleMovieResult() {
        $(function() {
            var movieId = getURLParameter('id');
            $.ajax({
                type: 'GET',
                url: 'getmovie?id=' + movieId,
                dataType: 'json',
                success: function(movie){
                    $.each(movie.genres, function(i, genre) {
                        movieGenreIds.push(genre.id);
                    });
                    var thisGenre = getMovieGenreName(movieGenreIds);
                    $('<div>').append('<img src="http://image.tmdb.org/t/p/w185/' + movie.poster_path + '">').appendTo('#thumbnail');
                    $('<div>').text(movie.title).appendTo('#title');
                    $('<div>').text(thisGenre).appendTo('#genres');
                    $('<div>').text(movie.release_date).appendTo('#release');
                    $('<div>').text(movie.vote_average).appendTo('#rating');
                    $('<div>').text(movie.overview).appendTo('#overview');
                    $('<div>').text(movie.runtime + " minutes").appendTo('#runtime');
                    $('<div>').text(movie.status).appendTo('#status');
                    if (!likedMovieExist) {
                        $('<div id="likebtn" class="fa fa-heart-o" value="' + movie.id + '"></div>').appendTo('#like');
                    } else {
                        $('<div id="likebtn" class="fa fa-heart red-clr" value="' + movie.id + '"></div>').appendTo('#like');
                    }
                }
            });
        });
    }

    function likeMovie(object) {
        if (object.hasClass("fa-heart-o")) {
            saveLikedMovie(object.attr('value'), 123);
            object.removeClass("fa-heart-o").addClass("fa-heart").addClass("red-clr");
        } else {
            deleteLikedMovie(object.attr('value'), 123);
            object.removeClass("fa-heart").removeClass("red-clr").addClass("fa-heart-o");
        }
    }

    $("#like").delegate("#likebtn", "click", function() {
        likeMovie($(this));
    });

    function saveLikedMovie(movieId, userId){
        $(function(){
            $.ajax({
                type: 'GET',
                url: "savelikedmovie?movieid=" + movieId + "&userid=" + userId,
            });
        });
    }

    function deleteLikedMovie(movieId, userId) {
        $(function(){
            $.ajax({
                type: 'GET',
                url: "deletelikedmovie?movieid=" + movieId + "&userid=" + userId,
            });
        });
    }

    function checkLikedMovie(movieId, userId) {
        $(function() {
            $.ajax({
                type: 'GET',
                url: "getlikedmovie?userid=" + userId,
                dataType: 'json',
                success: function(result){
                    $.each(result.likedMovies, function(i, likedMovie) {
                        if (likedMovie.movieId == movieId) {
                            likedMovieExist = true;
                        }
                    });
                }
            });
        });
    }

    function fillGenreArray() {
        $(function() {
            $.ajax({
                type: 'GET',
                url: 'getgenrelist',
                dataType: 'json',
                success: function(result){
                    $.each(result.genres, function(i, genre) {
                        var genrePair = new Array();
                        genreIds[i] = genre.id;
                        genreNames[i] = genre.name;
                        genrePair[0] = genreIds[i];
                        genrePair[1] = genreNames[i];
                        genreArray.push(genrePair);
                    });
                }
            });
        });
    }

    function getMovieGenreName(movieGenreId) {
        var movieGenreStr = "";

        for (i = 0; i < movieGenreId.length; i++) {
            for (j = 0; j < genreArray.length; j++) {
                if (genreArray[j][0] == movieGenreId[i]) {
                    movieGenreStr += genreArray[j][1];
                }
            }
            if (i != movieGenreId.length - 1) {
                movieGenreStr += ", ";
            }
        }
        return movieGenreStr;
    }

});