package com.project.AdmoDb.controller;

import com.project.AdmoDb.entity.MovieRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = MovieViewController.class)
public class MovieViewControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MovieRepository movieRepository;


    @Test
    public void getAllGenres() throws Exception {
        this.mockMvc.perform(get("/genrelist"))
                .andExpect(view().name("genre"));
    }

    @Test
    public void getMovieAdventure() throws Exception {
        this.mockMvc.perform(get("/movielist?genre=12&page=1"))
                .andExpect(view().name("movieResult"));
    }

    @Test
    public void getSingleMovie() throws Exception {
        this.mockMvc.perform(get("/movie?id=447404"))
                .andExpect(view().name("movie"));
    }

    @Test
    public void getLikedMovie() throws Exception {
        this.mockMvc.perform(get("/likedmovie?userid=123"))
                .andExpect(view().name("likedmovie"));
    }

}
